var express 	= require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');

var jwt    = require('jsonwebtoken');
var config = require('./config');

var user = require('./routes/user.js');
var expense = require('./routes/expense.js');
var address = require('./routes/address.js');
var schoolSettings = require('./routes/school-settings.js');
var section = require('./routes/section.js');
var session = require('./routes/session.js');

var port = process.env.PORT || config.serverport;

mongoose.connect(config.database, function(err){
	if(err){
		console.log('Error connecting database, please check if MongoDB is running.');
	}else{
		console.log('Connected to database...');
	}
});

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: true }));
app.use(require('body-parser').json({ type : '*/*' }));

// use morgan to log requests to the console
app.use(morgan('dev'));

// Enable CORS from client-side
app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials");
  res.setHeader("Access-Control-Allow-Credentials", "true");
  next();
});

// basic routes

app.get('/', function(req, res) {
	res.send('Expense Watch API is running at http://localhost:' + port + '/api');
});

app.post('/register', user.signup);

// express router
var apiRoutes = express.Router();

app.use('/api', apiRoutes);

apiRoutes.post('/login', user.login);

apiRoutes.use(user.authenticate); // route middleware to authenticate and check token

// authenticated routes
apiRoutes.get('/', function(req, res) {
	res.status(201).json({ message: 'Welcome to the authenticated routes!' });
});

apiRoutes.get('/user/:id', user.getuserDetails); // API returns user details

apiRoutes.put('/user/:id', user.updateUser); // API updates user details

apiRoutes.put('/password/:id', user.updatePassword); // API updates user password

apiRoutes.post('/expense/:id', expense.saveexpense); // API adds & update expense of the user

apiRoutes.delete('/expense/:id', expense.delexpense); //API removes the expense details of given expense id

apiRoutes.get('/expense/:id', expense.getexpense); // API returns expense details of given expense id

apiRoutes.post('/expense/total/:id', expense.expensetotal); // API returns expense details of given expense id

apiRoutes.post('/expense/report/:id', expense.expensereport); //API returns expense report based on user input
/******** address  ***********/

apiRoutes.post('/address/:id', address.saveaddress);
apiRoutes.delete('/address/:id', address.deladdress);
apiRoutes.get('/address/:id', address.getaddress);
apiRoutes.post('/address/list/:id', address.allAddresses);
apiRoutes.get('/addressDropdown/:id', address.addressDropdown);

/*********** School Settings  **********/
apiRoutes.post('/school-settings/:id', schoolSettings.saveaddress);
apiRoutes.get('/school-settings/:id', schoolSettings.getSettings);
apiRoutes.get('/school-settings/join/:id', schoolSettings.getSettings_join);

/*********** section **********/
apiRoutes.post('/section/:id', section.saveSection);
apiRoutes.delete('/section/:id', section.delSection);
apiRoutes.get('/section/:id', section.getSection);
apiRoutes.post('/section/list/:id', section.allSections);

/*********** session **********/
apiRoutes.post('/session/:id', session.saveSession);
apiRoutes.delete('/session/:id', session.delSession);
apiRoutes.get('/session/:id', session.getSession);
apiRoutes.post('/session/list/:id', session.allSessions);

// kick off the server
app.listen(port);
console.log('Expense Watch app is listening at http://localhost:' + port);
