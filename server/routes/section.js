var mongoose = require('mongoose');
var Section = require('../models/section');
var config = require('../config');

exports.saveSection = function(req, res, next) {
  const uid = req.params.id;
  const sectionName = req.body.sectionName;
  const sectionId = req.body.sectionId;

  if (!sectionName) {
    return res.status(422).send({
      success: false,
      message: 'Posted data is not correct or incompleted.'
    });
  } else {

    if (sectionId) {
      //Edit expense
      Section.findById(sectionId).exec(function(err, section) {
        if (err) {
          res.status(400).json({
            success: false,
            message: 'Error processing request ' + err
          });
        }

        if (section) {
          section.sectionName = sectionName;
        }
        section.save(function(err) {
          if (err) {
            res.status(400).json({
              success: false,
              message: 'Error processing request ' + err
            });
          }
          res.status(201).json({
            success: true,
            message: 'Section updated successfully'
          });
        });
      });

    } else {

      // Add new Section
      let oSection = new Section({
        sectionName: sectionName,
      });

      oSection.save(function(err) {
        if (err) {
          res.status(400).json({
            success: false,
            message: 'Error processing request ' + err
          });
        }

        res.status(201).json({
          success: true,
          message: 'Section saved successfully'
        });
      });

    }
  }
}

exports.delSection = function(req, res, next) {
  Section.remove({
    _id: req.params.id
  }, function(err) {
    if (err) {
      res.status(400).json({
        success: false,
        message: 'Error processing request ' + err
      });
    }
    res.status(201).json({
      success: true,
      message: 'Section removed successfully'
    });
  });
}

exports.getSection = function(req, res, next) {
  Section.find({
    _id: req.params.id
  }).exec(function(err, section) {
    if (err) {
      res.status(400).json({
        success: false,
        message: 'Error processing request ' + err
      });
    }
    res.status(201).json({
      success: true,
      data: section
    });
  });
}


exports.allSections = function(req, res, next) {
  const uid = req.params.id || req.query.uname;

  let limit = parseInt(req.query.limit);
  let page = parseInt(req.body.page || req.query.page);
  let sortby = req.body.sortby || req.query.sortby;
  let query = {};

  if (!limit || limit < 1) {
    limit = 2;
  }

  if (!page || page < 1) {
    page = 1;
  }

  if (!sortby) {
    sortby = 'sectionName';
  }

  var offset = (page - 1) * limit;

  if (!uid) {
    return res.status(422).send({
      error: 'Posted data is not correct or incompleted.'
    });
  } else {


    // returns all expense records for the user
    query = {

    };

    Section.count(query, function(err, count) {
      if (count > offset) {
        offset = 0;
      }
    });


    var options = {
      select: '',
      sort: sortby,
      offset: offset,
      limit: limit
    }

    Section.paginate(query, options).then(function(result) {
      res.status(201).json({
        success: true,
        data: result
      });
    });
  }
}
