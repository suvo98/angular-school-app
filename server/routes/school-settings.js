var mongoose = require( 'mongoose' );
var MJ = require("mongo-fast-join"),
    mongoJoin = new MJ();
var SchoolSettings = require('../models/school-settings');
var Address = require('../models/address');
var config = require('../config');

exports.saveaddress = function(req, res, next){

    const addressId = req.body.addressId;
    const name = req.body.name;
    const principal = req.body.principal;
    const details = req.body.details;
    const settingsId = req.body.settingsId;

    if (!addressId || !name || !principal || !details ) {
        return res.status(422).send({ success: false, message: 'Posted data is not correct or incompleted.' });
    } else {

	if (settingsId) {
		//Edit expense
		SchoolSettings.findById(settingsId).exec(function(err, settings){
			if(err){ res.status(400).json({ success: false, message: 'Error processing request '+ err }); }

			if(settings) {
				settings.addressId = addressId;
				settings.name = name;
				settings.principal = principal;
				settings.details = details;
			}
			settings.save(function(err) {
				if(err){ res.status(400).json({ success: false, message: 'Error processing request '+ err }); }
				res.status(201).json({
					success: true,
					message: 'School Settings updated successfully'
				});
			});
		});

	}else{

		// Add new Address
		let oSettings = new SchoolSettings({
			addressId: addressId,
			name: name,
			principal: principal,
			details: details,
		});

		oSettings.save(function(err) {
			if(err){ res.status(400).json({ success: false, message: 'Error processing request '+ err }); }

			res.status(201).json({
				success: true,
				message: 'School Settings saved successfully'
			});
		});

	}
    }
}


exports.getSettings = function(req, res, next){
	SchoolSettings.find({_id:req.params.id}).exec(function(err, settings){
        if(err){ res.status(400).json({ success: false, message:'Error processing request '+ err });
        }
        res.status(201).json({
      		success: true,
      		data: settings
      	});
    });
}


exports.getSettings_join = function(req, res, next){

  var MongoClient = require('mongodb').MongoClient;
  var url = "mongodb://localhost:27017/expensedb";

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    db.collection('school_settings').aggregate([

      { $lookup:
         {
           from: 'addresses',
           localField: 'addressId',
           foreignField: '_id',
           as: 'orderdetails'
         }
       }
      ], function(err, res) {
      if (err) throw err;
      console.log(JSON.stringify(res));
      db.close();
    });
  });
}
