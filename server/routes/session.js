var mongoose = require('mongoose');
var Session = require('../models/session');
var config = require('../config');

exports.saveSession = function(req, res, next) {
  const uid = req.params.id;
  const sessionName = req.body.sessionName;
  const sessionId = req.body.sessionId;

  if (!sessionName) {
    return res.status(422).send({
      success: false,
      message: 'Posted data is not correct or incompleted.'
    });
  } else {

    if (sessionId) {
      //Edit Session
      Session.findById(sessionId).exec(function(err, session) {
        if (err) {
          res.status(400).json({
            success: false,
            message: 'Error processing request ' + err
          });
        }

        if (session) {
          session.sessionName = sessionName;
        }
        session.save(function(err) {
          if (err) {
            res.status(400).json({
              success: false,
              message: 'Error processing request ' + err
            });
          }
          res.status(201).json({
            success: true,
            message: 'Session updated successfully'
          });
        });
      });

    } else {

      // Add new Session
      let oSession = new Session({
        sessionName: sessionName,
      });

      oSession.save(function(err) {
        if (err) {
          res.status(400).json({
            success: false,
            message: 'Error processing request ' + err
          });
        }

        res.status(201).json({
          success: true,
          message: 'Session saved successfully'
        });
      });

    }
  }
}

exports.delSession = function(req, res, next) {
  Session.remove({
    _id: req.params.id
  }, function(err) {
    if (err) {
      res.status(400).json({
        success: false,
        message: 'Error processing request ' + err
      });
    }
    res.status(201).json({
      success: true,
      message: 'Session removed successfully'
    });
  });
}

exports.getSession = function(req, res, next) {
  Session.find({
    _id: req.params.id
  }).exec(function(err, session) {
    if (err) {
      res.status(400).json({
        success: false,
        message: 'Error processing request ' + err
      });
    }
    res.status(201).json({
      success: true,
      data: session
    });
  });
}


exports.allSessions = function(req, res, next) {
  const uid = req.params.id || req.query.uname;

  let limit = parseInt(req.query.limit);
  let page = parseInt(req.body.page || req.query.page);
  let sortby = req.body.sortby || req.query.sortby;
  let query = {};

  if (!limit || limit < 1) {
    limit = 2;
  }

  if (!page || page < 1) {
    page = 1;
  }

  if (!sortby) {
    sortby = 'sectionName';
  }

  var offset = (page - 1) * limit;

  if (!uid) {
    return res.status(422).send({
      error: 'Posted data is not correct or incompleted.'
    });
  } else {


    // returns all expense records for the user
    query = {

    };

    Session.count(query, function(err, count) {
      if (count > offset) {
        offset = 0;
      }
    });


    var options = {
      select: '',
      sort: sortby,
      offset: offset,
      limit: limit
    }

    Session.paginate(query, options).then(function(result) {
      res.status(201).json({
        success: true,
        data: result
      });
    });
  }
}
