var mongoose = require( 'mongoose' );
var Address = require('../models/address');
var config = require('../config');

exports.saveaddress = function(req, res, next){
    const uid = req.params.id;
    const addressDetails = req.body.addressDetails;
    const addressid = req.body.addressid;

    if (!addressDetails) {
        return res.status(422).send({ success: false, message: 'Posted data is not correct or incompleted.' });
    } else {

	if (addressid) {
		//Edit expense
		Address.findById(addressid).exec(function(err, address){
			if(err){ res.status(400).json({ success: false, message: 'Error processing request '+ err }); }

			if(address) {
				address.addressDetails = addressDetails;
			}
			address.save(function(err) {
				if(err){ res.status(400).json({ success: false, message: 'Error processing request '+ err }); }
				res.status(201).json({
					success: true,
					message: 'Address updated successfully'
				});
			});
		});

	}else{

		// Add new Address
		let oAddress = new Address({
			addressDetails: addressDetails,
		});

		oAddress.save(function(err) {
			if(err){ res.status(400).json({ success: false, message: 'Error processing request '+ err }); }

			res.status(201).json({
				success: true,
				message: 'Address saved successfully'
			});
		});

	}
    }
}

exports.deladdress = function(req, res, next) {
	Address.remove({_id: req.params.id}, function(err){
        if(err){ res.status(400).json({ success: false, message: 'Error processing request '+ err }); }
        res.status(201).json({
		success: true,
		message: 'Address removed successfully'
	});
    });
}

exports.getaddress = function(req, res, next){
	Address.find({_id:req.params.id}).exec(function(err, address){
        if(err){ res.status(400).json({ success: false, message:'Error processing request '+ err });
        }
        res.status(201).json({
		success: true,
		data: address
	});
    });
}

exports.addresstotal = function(req, res, next){

    Address.aggregate([
        match,
        { "$group": {
            "_id": 1
        }}
    ],
    function(err, result) {
        if(err){ res.status(400).json({ success: false, message:'Error processing request '+ err }); }
        res.status(201).json({
		success: true,
		data: result
	});
    })

}

exports.allAddresses = function(req, res, next){
    const uid = req.params.id || req.query.uname;

    let limit = parseInt(req.query.limit);
    let page = parseInt(req.body.page || req.query.page);
    let sortby = req.body.sortby || req.query.sortby;
    let query = {};

    if(!limit || limit < 1) {
	     limit = 2;
    }

    if(!page || page < 1) {
	     page = 1;
    }

    if(!sortby) {
	     sortby = 'addressName';
    }

    var offset = (page - 1) * limit;

    if (!uid) {
        return res.status(422).send({ error: 'Posted data is not correct or incompleted.'});
	}else{


			// returns all expense records for the user
			query = {
        
       };

			Address.count(query, function(err, count){
				if(count > offset){
					offset = 0;
				}
			});


		var options = {
			select: '',
			sort: sortby,
			offset: offset,
			limit: limit
		}

		Address.paginate(query, options).then(function(result) {
			res.status(201).json({
				success: true,
				data: result
			});
		});
	}
}


exports.addressDropdown = function(req, res, next){
  Address.find({}, function (err, docs) {
          res.status(201).json({
            success: true,
            data: docs
          });
      });
}
