const mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema;

const AddressSchema = new Schema({
    addressDetails: {type:String, required: true}
});

AddressSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('addresses', AddressSchema, 'addresses');
