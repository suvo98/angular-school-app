const mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema;

const SessionSchema = new Schema({
    sessionName: {type:String, required: true}
});

SessionSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('sessions', SessionSchema, 'sessions');
