const mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema;

const schoolSettingsSchema = new Schema({
    addressId: {type:String, required: true},
    name: {type:String, required: true},
    principal: {type:String, required: true},
    details: {type:String, required: true}
});

schoolSettingsSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('school_settings', schoolSettingsSchema, 'school_settings');
