const mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema;

const SectionSchema = new Schema({
    sectionName: {type:String, required: true}
});

SectionSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('sections', SectionSchema, 'sections');
