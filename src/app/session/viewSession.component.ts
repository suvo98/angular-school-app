import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SessionService } from './session.service';
import { AuthService } from '../user/auth.service';
import { ToastrService } from '../common/toastr.service';
import { Observable } from 'rxjs/Observable'
import { Subscription } from 'rxjs/Subscription';
import { ISession } from './session';

@Component({
  templateUrl: './viewSession.component.html'
})

export class viewSessionComponent implements OnInit, OnDestroy {

  expense: ISession;
  private sub: Subscription;

  constructor(
    private authService: AuthService,
    private expenseService: SessionService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(
      params => {
        let id = params['id'];
        this.getAddress(id);
      });
    }

    ngOnDestroy() {
      this.sub.unsubscribe();
    }

    getAddress(id){
      this.expenseService.getSession(id).subscribe(data => {
        if (data.success === false) {
          if (data.errcode){
            this.authService.logout();
            this.router.navigate(['login']);
          }
          this.toastr.error(data.message);
        } else {
          if (data.data[0]) {
            this.expense = data.data[0];
            console.log(this.expense)
          } else {
            this.toastr.error('Session id is incorrect in the URL');
          }

        }
      });
    }

    onBack(): void {
        this.router.navigate(['/session/list'], { queryParamsHandling: "merge" });
    }
}
