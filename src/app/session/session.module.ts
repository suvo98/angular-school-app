import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';


import { SessionListComponent } from './session-list.component'
import { SessionComponent } from './session.component';
import { viewSessionComponent } from './viewSession.component';

import { SessionService } from './session.service';

import { AuthGuard } from '../user/auth-guard.service';
import { AuthService } from '../user/auth.service';



@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: 'session/list', canActivate: [ AuthGuard], component: SessionListComponent },
      { path: 'create/new/session', canActivate: [ AuthGuard], component: SessionComponent },
      { path: 'update/session/:id', canActivate: [ AuthGuard], component: SessionComponent },
      { path: 'view/session/:id', canActivate: [ AuthGuard], component: viewSessionComponent },

      // { path: 'expense/view/:id', canActivate: [ AuthGuard], component: ViewexpenseComponent }
    ])
  ],
  declarations: [
    SessionListComponent,
    SessionComponent,
    viewSessionComponent
  ],
  providers: [
    DatePipe,
    AuthService,
    AuthGuard,
    SessionService
  ]
})
export class SessionModule {}
