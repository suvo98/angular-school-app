import { NgModule } from '@angular/core';
import { Routes , RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SectionComponent } from './main-body/section/section.component';
import { SchoolInfoComponent } from './main-body/school-info/school-info.component';
import { SchoolAddressesComponent } from './main-body/school-addresses/school-addresses.component';
// import { LoginComponent } from './login/login.component';
import { LoginComponent } from './home/login.component';
import { AuthGuard } from './user/auth-guard.service';

const appRoutes: Routes = [ 
  {path: '' , canActivate: [ AuthGuard] , component: SectionComponent},
  {path: 'login' , component: LoginComponent , resolve: [AuthGuard] },
  // {path: '' , redirectTo: 'login' , pathMatch: 'full'},
  // {path: '**' , redirectTo: 'login' , pathMatch: 'full'},
  {path: 'section' , canActivate: [ AuthGuard], component: SectionComponent},
  {path: 'school-info' , canActivate: [ AuthGuard], component: SchoolInfoComponent},
  {path: 'school-addresses' , canActivate: [ AuthGuard], component: SchoolAddressesComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
