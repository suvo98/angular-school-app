import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';


import { SettingsComponent } from './settings.component'

import { SchoolSettingsService } from './settings.service';

import { AuthGuard } from '../user/auth-guard.service';
import { AuthService } from '../user/auth.service';



@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: 'update/settings/:id', canActivate: [ AuthGuard], component: SettingsComponent },
      { path: 'expense/:id', canActivate: [ AuthGuard], component: SettingsComponent },
      // { path: 'expense/view/:id', canActivate: [ AuthGuard], component: ViewexpenseComponent }
    ])
  ],
  declarations: [
    SettingsComponent
  ],
  providers: [
    DatePipe,
    AuthService,
    AuthGuard,
    SchoolSettingsService,
  ]
})
export class SettingsModule {}
