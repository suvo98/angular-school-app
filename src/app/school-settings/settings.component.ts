import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe , Location } from '@angular/common';
import { ToastrService } from '../common/toastr.service'
import { SchoolSettingsService } from './settings.service';
import { AuthService } from '../user/auth.service';

import { DropdownService } from '../common/dropdown.services';
import { IAddress } from '../address/address';

@Component({
  templateUrl: './settings.component.html'
})

export class SettingsComponent implements OnInit {

  addressForm: FormGroup;
  userObj: any;
  expid: string;
  pgTitle: string;
  btnLbl: string;
  addresses: IAddress[];

  constructor(private fb: FormBuilder,
    private authService: AuthService,
    private expenseService: SchoolSettingsService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private datePipe: DatePipe,
    private location: Location,
    private dropdown: DropdownService) {
  }

  addressId = new FormControl('', [Validators.required]);
  name = new FormControl('', [Validators.required]);
  principal = new FormControl('', [Validators.required]);
  details = new FormControl('', [Validators.required]);

  ngOnInit(){
    this.userObj =  this.authService.currentUser;
    this.dropdown.addressDropdown(this.userObj.userid).subscribe(

      data => {
        this.addresses = data.data; //Fetching available HMOs
        
      }
    );

    this.route.params.subscribe((params: any) => {
      if (params.id) {
        this.expid = params['id'];
        this.getExpense(this.expid);
        this.pgTitle = "Edit";
        this.btnLbl = "Update"
      } else {
        this.pgTitle = "Add";
        this.btnLbl = "Submit"
      }
    });


    this.addressForm = this.fb.group({
      addressId: this.addressId,
      name: this.name,
      principal: this.principal,
      details: this.details,
    });
  }

  getExpense(id){
    this.expenseService.getAddress(id).subscribe(data => {
      if (data.success === true) {
        if (data.data[0]) {
          this.populateForm(data.data[0]);
        } else {
          this.toastr.error('Expense id is incorrect in the URL');
          // this.router.navigate(['report']);
        }
      }
    });
  }

  populateForm(data): void {
    this.addressForm.patchValue({
      addressId: data.addressId,
      name: data.name,
      principal: data.principal,
      details: data.details,
    });
  }

  saveAddress(formdata:any): void {
    if (this.addressForm.valid) {
      const theForm = this.addressForm.value;
      if (this.expid !== '') {
        theForm.settingsId = this.expid;
      }

      this.expenseService.saveAddress(this.userObj.userid,theForm)
        .subscribe(data => {
          if (data.success === false) {
            if (data.errcode){
              this.authService.logout();
              this.router.navigate(['login']);
            }
            this.toastr.error(data.message);
          } else {
            this.toastr.success(data.message);
          }
          if (!this.expid) {
            this.addressForm.reset();
          }
      });
    }
  }

  onBack(): void {
    // this.router.navigate(['/address/list'], { preserveQueryParams: true });
    this.location.back();
  }

}
