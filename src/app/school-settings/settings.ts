export interface IAddress {
  addressId: string;
  name: string;
  principal: string;
  details: string;
}
