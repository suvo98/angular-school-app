import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SectionService } from './section.service';
import { AuthService } from '../user/auth.service';
import { ToastrService } from '../common/toastr.service';
import { Observable } from 'rxjs/Observable'
import { Subscription } from 'rxjs/Subscription';
import { ISection } from './section';

@Component({
  templateUrl: './viewSection.component.html'
})

export class viewSectionComponent implements OnInit, OnDestroy {

  expense: ISection;
  private sub: Subscription;

  constructor(
    private authService: AuthService,
    private expenseService: SectionService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(
      params => {
        let id = params['id'];
        this.getAddress(id);
      });
    }

    ngOnDestroy() {
      this.sub.unsubscribe();
    }

    getAddress(id){
      this.expenseService.getSection(id).subscribe(data => {
        if (data.success === false) {
          if (data.errcode){
            this.authService.logout();
            this.router.navigate(['login']);
          }
          this.toastr.error(data.message);
        } else {
          if (data.data[0]) {
            this.expense = data.data[0];
          } else {
            this.toastr.error('Section id is incorrect in the URL');
          }

        }
      });
    }

    onBack(): void {
        this.router.navigate(['/section/list'], { queryParamsHandling: "merge" });
    }
}
