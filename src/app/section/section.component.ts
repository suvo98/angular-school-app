import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ToastrService } from '../common/toastr.service'
import { SectionService } from './section.service';
import { AuthService } from '../user/auth.service';

@Component({
  templateUrl: './section.component.html'
})

export class SectionComponent implements OnInit {

  sectionForm: FormGroup;
  userObj: any;
  expid: string;
  pgTitle: string;
  btnLbl: string;

  constructor(private fb: FormBuilder,
    private authService: AuthService,
    private expenseService: SectionService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private datePipe: DatePipe) {
  }

  sectionName = new FormControl('', [Validators.required]);

  ngOnInit(){

    this.route.params.subscribe((params: any) => {
      if (params.id) {
        this.expid = params['id'];
        this.getExpense(this.expid);
        this.pgTitle = "Edit";
        this.btnLbl = "Update"
      } else {
        this.pgTitle = "Add";
        this.btnLbl = "Submit"
      }
    });

    this.userObj =  this.authService.currentUser;
    this.sectionForm = this.fb.group({
      sectionName: this.sectionName
    });
  }

  getExpense(id){
    this.expenseService.getSection(id).subscribe(data => {
      if (data.success === true) {
        if (data.data[0]) {
          this.populateForm(data.data[0]);
        } else {
          this.toastr.error('Expense id is incorrect in the URL');
          this.router.navigate(['report']);
        }
      }
    });
  }

  populateForm(data): void {
    this.sectionForm.patchValue({
      sectionName: data.sectionName
    });
  }

  saveSection(formdata:any): void {
    if (this.sectionForm.valid) {
      const theForm = this.sectionForm.value;
      if (this.expid !== '') {
        theForm.sectionId = this.expid;
      }

      this.expenseService.saveSection(this.userObj.userid,theForm)
        .subscribe(data => {
          if (data.success === false) {
            if (data.errcode){
              this.authService.logout();
              this.router.navigate(['login']);
            }
            console.log(data.message)
            this.toastr.error(data.message);
          } else {
            this.toastr.success(data.message);
          }
          if (!this.expid) {
            this.sectionForm.reset();
          }
      });
    }
  }

  onBack(): void {
    this.router.navigate(['/section/list'], { preserveQueryParams: true });
  }

}
