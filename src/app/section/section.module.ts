import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';


import { SectionListComponent } from './section-list.component'
import { SectionComponent } from './section.component';
import { viewSectionComponent } from './viewSection.component';

import { SectionService } from './section.service';

import { AuthGuard } from '../user/auth-guard.service';
import { AuthService } from '../user/auth.service';



@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: 'section/list', canActivate: [ AuthGuard], component: SectionListComponent },
      { path: 'create/new/section', canActivate: [ AuthGuard], component: SectionComponent },
      { path: 'update/section/:id', canActivate: [ AuthGuard], component: SectionComponent },
      { path: 'view/section/:id', canActivate: [ AuthGuard], component: viewSectionComponent },
      { path: 'expense/:id', canActivate: [ AuthGuard], component: SectionComponent },
      // { path: 'expense/view/:id', canActivate: [ AuthGuard], component: ViewexpenseComponent }
    ])
  ],
  declarations: [
    SectionListComponent,
    SectionComponent,
    viewSectionComponent
  ],
  providers: [
    DatePipe,
    AuthService,
    AuthGuard,
    SectionService
  ]
})
export class SectionModule {}
