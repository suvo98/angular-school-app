import { Component, OnInit , OnDestroy  } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Observer } from 'rxjs/Observer'
import { Subscription } from 'rxjs/Subscription'
import 'rxjs/add/operator/map'

import { DatePipe } from '@angular/common';
import { ToastrService } from '../common/toastr.service'
import { SectionService } from './section.service';
import { AuthService } from '../user/auth.service';
import { ISection } from './section';

@Component({
  templateUrl: './section-list.component.html'
})

export class SectionListComponent implements OnInit , OnDestroy {

  reportForm: FormGroup
  userObj: any;
  oberserverNumber: number;
  oberserverStr: string;
  title: string;
  expenses: ISection[];
  qstartdt: string;
  qenddt: string;
  exptotal: number;
  /****** for pagination *********/
  totalrows: number;
  pgCounter: number;
  qreport: string;
  qpage: number;
  qsort: string;
  setNextPagescon: boolean;
  setNextPageLi: number;
  setNextInitPageLi: number;
  setPrevPagescon: boolean;
  setPrevPageLi: number;
  setPrevInitPageLi: number;
  /****************************/

  numberSubscription : Subscription;
  strSubscription : Subscription;

  constructor(private fb: FormBuilder,
    private authService: AuthService,
    private expenseService: SectionService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private datePipe: DatePipe) {
  }

  report = new FormControl('opt1');
  startdt = new FormControl({value: '', disabled: true});
  enddt = new FormControl({value: '', disabled: true});

  ngOnDestroy(){
    this.strSubscription.unsubscribe();
    this.numberSubscription.unsubscribe();
  }

  ngOnInit(){
    /****** for pagination *********/
    this.setPrevPagescon = false;
    this.setNextPagescon = true;
    /************** number observable *****************/
    const myObservable = Observable.interval(1000);
    this.numberSubscription = myObservable.subscribe(
      (number : number) => {
        this.oberserverNumber = number;
      }
    );

    /****** my own observer ********/
    const myOwnObserver = Observable.create((observer: Observer<string>) => {
      setTimeout(() => {
        observer.next('first Package')
      },2000);
      setTimeout(() => {
        observer.next('Second Package')
      },4000);
      setTimeout(() => {
        observer.complete()
      },5000);
      setTimeout(() => {
        observer.next('Complete Package')
      },6000);
    });

    this.strSubscription = myOwnObserver.subscribe(
      (data: string) => { this.oberserverStr = data; },
      (error: string) => {this.oberserverStr = error; },
      () => { console.log('Completed'); }
    );

    /******************/

    this.userObj =  this.authService.currentUser;
    this.reportForm = this.fb.group({
      report: this.report,
      startdt: this.startdt,
      enddt: this.enddt
    });
    this.route.queryParams.forEach((params: Params) => {
      this.qpage = params['page'] || '';
        let payload: any = {};
        payload.page = this.qpage;
        this.fetchReport(this.userObj.userid, payload);
        // this.getReport();
    })


    /****** for pagination *********/
    this.route.queryParams.subscribe((params)=> {
      //check lead Id here
      if(params['page']){

      } else {
        const page = 1;
        this.router.navigate(['section/list'],
          {
            queryParams: {  page: page }
          }
        );
      }
    });
    /*****************************/

  }

  getReport() {

    this.fetchReport(this.userObj.userid, this.reportForm.value);
  }

  fetchReport(userid, formval) {
    this.expenseService.getAddresses(userid, formval)
    .subscribe(data => {
      if (data.success === false) {
        if (data.errcode){
          this.authService.logout();
          this.router.navigate(['login']);
        }
        this.toastr.error(data.message);
      } else {

        this.expenses = data.data.docs;
        this.totalrows = +data.data.total;
        this.pgCounter = Math.floor((this.totalrows + 2 - 1) / 2);
        this.title = 'All Sections'

      }
    });
  }

/****** for pagination *********/
  setPageNext(totalrows): void{
    const pageNumber = +this.route.snapshot.queryParams['page'];
    const modulusPage = pageNumber % 10;
    if((pageNumber+1) <= this.pgCounter){
      if(modulusPage == 0){
        this.setPrevPagescon = false;
        this.setNextPagescon = true;
        if(totalrows >= pageNumber){
          this.setNextInitPageLi = pageNumber + 1;
          this.setNextPageLi = pageNumber + 10;
        }
      }
      this.setPage(pageNumber+1);
    }
  }

  setPagePrev(totalrows): void{
    const pageNumber = +this.route.snapshot.queryParams['page'];
    const modulusPage = (pageNumber-1) % 10;
    if(pageNumber > 1){
      if(modulusPage == 0){
        this.setNextPagescon = false;
        this.setPrevPagescon = true;
          this.setPrevInitPageLi = pageNumber - 11;
          this.setPrevPageLi = pageNumber - 1;
      }
      this.setPage(pageNumber-1);
    }
  }

  setPage(page): void {
    this.setPrevPagescon = false;
    this.setNextPagescon = true;
    this.router.navigate(['section/list'],
      {
        queryParams: {  page: page }
      }
    );
  }

  createPager(number) {
    const pageNumber = +this.route.snapshot.queryParams['page'];
    const modulusPage = pageNumber % 10;
    var pageNumberStr = pageNumber.toString();
    const lastTwoLetter = +pageNumberStr.slice(-1);
    const pageInit = pageNumber - lastTwoLetter;

    if(modulusPage == 0){

      var items: number[] = [];
      this.setPrevPagescon = false;
      this.setNextPagescon = true;
      this.setNextInitPageLi = pageInit - 9;
      this.setNextPageLi = pageInit;

      for(var i = 1; i <= number; i++){
        items.push(i);
      }
      return items;
    }
    else if(pageNumber){

      var items: number[] = [];
      this.setPrevPagescon = false;
      this.setNextPagescon = true;
      this.setNextInitPageLi = pageInit + 1;
      this.setNextPageLi = pageInit + 10;

      for(var i = 1; i <= number; i++){
        items.push(i);
      }
      return items;
    }
    else{
      var items: number[] = [];

      for(var i = 1; i <= number; i++){
        items.push(i);
      }
      return items;
    }

  }

  /*************************/

  showExpense(expid): void {
    this.router.navigate([`view/section/${expid}`],
      {
        queryParams: {  }
      }
    );
  }

  confirmDel(idx: number, expid: string) {
    if(confirm('Do you really want to delete this record?')){
      this.expenseService.delSection(expid)
      .subscribe(data => {
        if (data.success === false) {
          if (data.errcode){
            this.authService.logout();
            this.router.navigate(['login']);
          }
          this.toastr.error(data.message);
        } else {

          this.expenses.splice(idx, 1);

          this.toastr.success(data.message);
        }
      });
    }
  }

  editExpense(expid): void {
    this.router.navigate([`update/section/${expid}`],
      {
        // queryParams: { report: this.qreport, startdt: this.qstartdt, enddt: this.qenddt, page: this.qpage || 1, sortby: this.qsort }
      }
    );
  }

  sortExpense(sortby): void {
    if (this.qsort === ''){
      this.qsort = sortby;
    } else if (this.qsort.indexOf('-') > -1 ) {
      this.qsort = sortby;
    } else {
      this.qsort = '-' + sortby;
    }

    this.router.navigate(['report'],
      {
        queryParams: { report: this.qreport, startdt: this.qstartdt, enddt: this.qenddt, page: this.qpage || 1, sortby: this.qsort }
      }
    );
  }

}
