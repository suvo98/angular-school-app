
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import 'rxjs/Rx';
import { AppComponent } from './app.component';
import { TopHeaderComponent } from './top-header/top-header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { SectionComponent } from './main-body/section/section.component';

import { AppRoutingModule } from './app-routing.module';
import { SchoolInfoComponent } from './main-body/school-info/school-info.component';
import { SchoolAddressesComponent } from './main-body/school-addresses/school-addresses.component';
// import { LoginComponent } from './login/login.component';

import { LoginComponent } from './home/login.component';

import { UserModule } from './user/user.module';
import { ExpenseModule } from './expense/expense.module';
import { AddressModule } from './address/address.module';
import { SettingsModule } from './school-settings/settings.module';
import { SectionModule } from './section/section.module';
import { SessionModule } from './session/session.module';

/* common Modules */
import { ToastrService } from './common/toastr.service';
import { AuthService } from './user/auth.service';
import { DropdownService } from './common/dropdown.services';

@NgModule({
  declarations: [
    AppComponent,
    TopHeaderComponent,
    SidebarComponent,
    FooterComponent,
    SectionComponent,
    SchoolInfoComponent,
    SchoolAddressesComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    UserModule,
    ExpenseModule,
    AddressModule,
    SettingsModule,
    SectionModule,
    SessionModule,
    ReactiveFormsModule,
    AppRoutingModule  
  ],
  providers: [ToastrService , AuthService , DropdownService],
  bootstrap: [AppComponent]
})
export class AppModule { }
