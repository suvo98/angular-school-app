import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AddressService } from './address.service';
import { AuthService } from '../user/auth.service';
import { ToastrService } from '../common/toastr.service';
import { Observable } from 'rxjs/Observable'
import { Subscription } from 'rxjs/Subscription';
import { IAddress } from './address';

@Component({
  templateUrl: './viewAddress.component.html'
})

export class viewAddressComponent implements OnInit, OnDestroy {

  expense: IAddress;
  private sub: Subscription;

  constructor(
    private authService: AuthService,
    private expenseService: AddressService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(
      params => {
        let id = params['id'];
        this.getAddress(id);
      });
    }

    ngOnDestroy() {
      this.sub.unsubscribe();
    }

    getAddress(id){
      this.expenseService.getAddress(id).subscribe(data => {
        if (data.success === false) {
          if (data.errcode){
            this.authService.logout();
            this.router.navigate(['login']);
          }
          this.toastr.error(data.message);
        } else {
          if (data.data[0]) {
            this.expense = data.data[0];
          } else {
            this.toastr.error('Expense id is incorrect in the URL');
          }

        }
      });
    }

    onBack(): void {
        this.router.navigate(['/address/list'], { queryParamsHandling: "merge" });
    }
}
