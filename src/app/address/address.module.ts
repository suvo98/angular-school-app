import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';


import { AddressListComponent } from './address-list.component'
import { AddressComponent } from './address.component';
import { viewAddressComponent } from './viewAddress.component';

import { AddressService } from './address.service';

import { AuthGuard } from '../user/auth-guard.service';
import { AuthService } from '../user/auth.service';

import { PaginationServiceService } from '../pagination-service.service';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: 'address/list', canActivate: [ AuthGuard], component: AddressListComponent },
      { path: 'create/new/address', canActivate: [ AuthGuard], component: AddressComponent },
      { path: 'update/address/:id', canActivate: [ AuthGuard], component: AddressComponent },
      { path: 'view/address/:id', canActivate: [ AuthGuard], component: viewAddressComponent },
      { path: 'expense/:id', canActivate: [ AuthGuard], component: AddressComponent },
      // { path: 'expense/view/:id', canActivate: [ AuthGuard], component: ViewexpenseComponent }
    ])
  ],
  declarations: [
    AddressListComponent,
    AddressComponent,
    viewAddressComponent
  ],
  providers: [
    DatePipe,
    AuthService,
    AuthGuard,
    AddressService,
    PaginationServiceService
  ]
})
export class AddressModule {}
