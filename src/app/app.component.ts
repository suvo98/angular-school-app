import { Component } from '@angular/core';
import { AuthService } from './user/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.css',
    // './styles.css',
    // './style-responsive.css',
  ]
})
export class AppComponent {
  title = 'app';

  constructor( public authService: AuthService, ) {

  }

}
