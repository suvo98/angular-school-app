import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.css']
})
export class SectionComponent implements OnInit {
  show_p: boolean;

  constructor() { }

  ngOnInit() {
    this.show_p = false;
  }

  hey(){
    this.show_p = true;
  }

}
