function delete_modal(elm){
   $('#delete_btn').attr('main_id',elm.attr('main_id'));
   $('#delete_modal').modal('show');
}

function delete_item(elm){

   $.ajax({
      type: 'POST',
       url: url+'/doctor/assistant/delete',
       data: {
           _token: $('#t').val(),
           id: elm.attr('main_id'),
       }
   }).done(function(){

       $('#delete_modal').modal('hide');
       location.reload();

   });

}
